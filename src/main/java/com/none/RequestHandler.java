package com.none;

import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


public class RequestHandler {

    private final String PATH = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private final String LANGUAGE = "en-ru";
    private String API_KEY = "trnsl.1.1.20180702T181515Z.ed2e9937b7937941.cd4eb16137f87d9dbca5bab6b15e5c624c193083\n";

    ClientHttpRequestFactory requestFactory;
    private RestTemplate template = new RestTemplate();
    Gson gson = new Gson();
    public String translate(String phrase)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("key", API_KEY);
        map.add("text", phrase);
        map.add("lang",LANGUAGE);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<String> response = template.postForEntity( PATH, request , String.class );

        ResponseModel responseModel = gson.fromJson(response.getBody(), ResponseModel.class);

        return String.join(" ", responseModel.getText());
    }
}
